package cn.com.chanyue;

import org.nutz.dao.Dao;
import org.nutz.dao.util.Daos;
import org.nutz.mvc.NutConfig;
import org.nutz.mvc.Setup;

public class UrlSetup implements Setup {

	public void init(NutConfig conf) {
		Daos.createTablesInPackage(conf.getIoc().get(Dao.class), getClass().getPackage().getName(), false);
	}
	
	public void destroy(NutConfig conf) {
	}

}
